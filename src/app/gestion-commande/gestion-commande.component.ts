import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  client: string;
  categorie: string;
  typerdz: string;
  daterdz: string;
  action: string;
  statut: string;
  agent: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {client: 'Client 1', categorie: 'Particulier', typerdz: 'Express', daterdz: 'JJ/MM/AAAA', action: 'A assigner', statut: 'Non assigné', agent: 'Affecter un agent'},
  {client: 'Client 2', categorie: 'Particulier', typerdz: 'Standard', daterdz: 'JJ/MM/AAAA', action: 'Assigné', statut: 'Assigné (pas encore demarré)', agent: 'Agent X'},
  {client: 'Client 3', categorie: 'Entreprise', typerdz: 'Standard', daterdz: 'JJ/MM/AAAA', action: 'Assigné', statut: 'En cours', agent: 'Agent Y'},
  {client: 'Client 4', categorie: 'Entreprises', typerdz: 'Express', daterdz: 'JJ/MM/AAAA', action: 'Cloturé', statut: 'Terminé', agent: 'Agent Z'},
  {client: 'Client 5', categorie: 'Particulier', typerdz: 'Express', daterdz: 'JJ/MM/AAAA', action: 'Assigné', statut: 'Annulé', agent: 'Agent'},
];

@Component({
  selector: 'app-gestion-commande',
  templateUrl: './gestion-commande.component.html',
  styleUrls: ['./gestion-commande.component.scss']
})
export class GestionCommandeComponent implements OnInit {

  displayedColumns: string[] = ['client', 'categorie', 'typerdz', 'daterdz', 'action', 'statut', 'agent' ];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
