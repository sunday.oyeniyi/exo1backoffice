import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableauBordComponent } from './tableau-bord/tableau-bord.component';
import { GestionCommandeComponent } from './gestion-commande/gestion-commande.component';
import { GestionClientComponent } from './gestion-client/gestion-client.component';
import { GestionAbonnementComponent } from './gestion-abonnement/gestion-abonnement.component';
import { AdministrationUserComponent } from './administration-user/administration-user.component';

const routes: Routes = [
  { path: 'tableau-bord', component: TableauBordComponent },
  { path: 'gestion-commande', component: GestionCommandeComponent },
  { path: 'gestion-abonnement', component: GestionAbonnementComponent },
  { path: 'gestion-client', component: GestionClientComponent },
  { path: 'administration-user', component: AdministrationUserComponent },
  { path: '',
    redirectTo: '/tableau-bord',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
