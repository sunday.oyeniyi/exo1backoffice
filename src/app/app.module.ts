import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBackOfficeComponent } from './nav-back-office/nav-back-office.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,
        MatListModule, MatTabsModule, MatGridListModule, MatCheckboxModule,
        MatRadioModule, MatInputModule, MatTableModule } from '@angular/material';
import { GestionCommandeComponent } from './gestion-commande/gestion-commande.component';
import { TableauBordComponent } from './tableau-bord/tableau-bord.component';
import { GestionAbonnementComponent } from './gestion-abonnement/gestion-abonnement.component';
import { GestionClientComponent } from './gestion-client/gestion-client.component';
import { AdministrationUserComponent } from './administration-user/administration-user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBackOfficeComponent,
    GestionCommandeComponent,
    TableauBordComponent,
    GestionAbonnementComponent,
    GestionClientComponent,
    AdministrationUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatGridListModule,
    MatCheckboxModule,
    MatRadioModule,
    MatInputModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
